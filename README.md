# Galley plugin for CakePHP

Provides a small Docker dev environment for CakePHP development.

## Introduction

Galley provides a Docker powered local development experience for CakePHP that is compatible with macOS, Windows (WSL2), and Linux. Other than Docker, no software or libraries are required to be installed on your local computer before using Galley. Galley's simple CLI means you can start building your CakePHP application without any previous Docker experience.

### Inspiration

Galley is inspired by and derived from [Vessel](https://github.com/shipping-docker/vessel) by [Chris Fidao](https://github.com/fideloper) as well as [Laravel Sail](https://github.com/laravel/laravel). If you're looking for a thorough introduction to Docker, check out Chris' course: [Shipping Docker](https://serversforhackers.com/shipping-docker).

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```sh
composer require --dev amayer5125/galley
```

Next [load the plugin](https://book.cakephp.org/4/en/plugins.html#loading-a-plugin) in your src/Application.php file.

```php
$this->addOptionalPlugin('Galley');
```

After you have the plugin installed you can initalize Galley by running the following:

```sh
bin/cake galley install
```

This will create a docker-composer.yml file in your project's root directory. After following the directions output by the command, you can start your application by running:

```sh
vendor/bin/galley up -d
```

Head to `http://localhost` in your browser and see your CakePHP site!

### Optional Setup

Instead of repeatedly typing `vendor/bin/galley` to execute Galley commands, you may wish to configure a Bash or ZSH alias that allows you to execute Galley's commands more easily:

```bash
alias galley="vendor/bin/galley"
```

## Common Commands

Here's a list of built-in helpers you can use. Any command not defined in the `vendor/bin/galley` script will default to being passed to the `docker-compose` command. If no arguments are used, it will run `docker-compose ps` to list the running containers for this environment.

### Starting and Stopping Galley

```bash
# Start the environment
vendor/bin/galley up -d

# Stop the environment
vendor/bin/galley stop

# Destroy the environment
vendor/bin/galley down
```

### Development

```bash
# Use composer
vendor/bin/galley composer <cmd>

# Use bin/cake
vendor/bin/galley bin/cake <cmd>
vendor/bin/galley cake <cmd> # "cake" is a shortcut to "bin/cake"

# Run npm
vendor/bin/galley npm <cmd>

## Example: install deps
vendor/bin/galley npm install
```

### Docker Commands

As mentioned, anything not recognized as a built-in command will be used as an argument for the `docker-compose` command. Here's a few handy tricks:

```bash
# Both will list currently running containers and their statuses
vendor/bin/galley
vendor/bin/galley ps

# Check log output of a container service
vendor/bin/galley logs # all container logs
vendor/bin/galley logs app # nginx | php logs
vendor/bin/galley logs mysql # mysql logs
vendor/bin/galley logs redis # redis logs

# Tail the logs to see output as it's generated
vendor/bin/galley logs -f # all logs
vendor/bin/galley logs -f app # nginx | php logs

# Start a bash shell inside the app container
# This is just like SSH'ing into a server
# Note that changes to a container made this way will **NOT**
# survive through stopping and starting the Galley environment.
# To install software or change server configuration, you'll need to
# edit the Dockerfile and run: vendor/bin/galley build
vendor/bin/galley shell

# Example: mysqldump database "my_app" to local file system
# We must add the password in the command line this way
# This creates the file "my_app.sql" on your local file system, not
# inside of the container
# @link https://serversforhackers.com/c/mysql-in-dev-docker
vendor/bin/galley exec mysql mysqldump -u root -psecret my_app > my_app.sql
```
